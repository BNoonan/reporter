

import sys
from tkinter import *

import os
import json
import importlib
import time
import pandas as pd
import re
import numpy as np
import shutil

import LIDAR_plotter as pl
import LIDAR_latex_writer as lw

import datetime as dt
from tkinter import *
from tkinter import filedialog


# 20190122 - added functionality to import different sitedetails file
# 20190122 - added new template just for LIDAR, no met, no validation


def import_data_and_plot():
    ''' 

    '''
    run_full_report = input("Full report? (y/n): ")
    monthly_or_period = input("Run monthly or period report? (m/p): ")
    run_validation = input("Validate data? (y/n): ")
    run_met =  input("Include External Met data? (y/n): ")

    
    if run_full_report == 'y':
    
        # CHECK DATES ARE VALID - TO DO - REMOVE
        dates_OK = 0
        dates = ['2017','06','01','2017','06','30']
        date_start = time.strptime(str(dates[2]) + '/' + str(dates[1]) + '/' + str(dates[0]), "%d/%m/%Y")
        date_end = time.strptime(str(dates[5]) + '/' + str(dates[4]) + '/' + str(dates[3]), "%d/%m/%Y")
        dates_OK = 1
    
        # IF DATES OK, IMPORT DATA
        if dates_OK == 1:
            data_imported = 0
    
            # LIDAR DATA
            master = Tk()
            master.withdraw() #hiding tkinter window for some reason?
            path = filedialog.askopenfilename(initialdir = os.getcwd() + '/Config_files/', title="Select site file", filetypes=[("json file","*.json"),("All files",".*")])
            site_Name = os.path.basename(path)

            with open('./Config_files/' + site_Name) as config_file:    
                config_LIDAR = json.load(config_file)
    
            try:                
                ID = importlib.import_module('import_' + config_LIDAR['Logger Type'])
                #ID = importlib.import_module('import_' + config_LIDAR['Logger Type'], package = 'import_data')               
                DATA = ID.import_data(config_LIDAR['Data Folder'],dates)
    
                DATA.index.name ='Timestamp'
                data_imported = 1
    
            except Exception as e: 
                print('ERROR: Data importation error \n')
                print(str(e))
    
            # Scale Air pressure from kPa to hPa
            DATA['Pressure (hPa)'] = DATA['AP_2'] / 10
            print('')

            

    
    
            # IMPORT MET DATA
            site_Name = config_LIDAR["Meteorological"]
            with open('./Config_files/SiteDetails_' + site_Name + '.json') as config_file:    
                config_met = json.load(config_file)
    
            try:
                ID = importlib.import_module('import_' + config_met['Logger Type'])
                #ID = importlib.import_module('import_' + config_met['Logger Type'], package = 'none')               
                M_DATA = ID.import_data(config_met['Data Folder'],dates)
    
                M_DATA.index.name ='Timestamp'
                data_imported += data_imported
    
            except Exception as e: 
                print('ERROR: Data importation error \n')
                print(str(e))
            print('')




    
    
            # IMPORT VALIDATION DATA
            site_Name = config_LIDAR["Validation"]
            with open('./Config_files/SiteDetails_'+ site_Name + '.json') as config_file:    
                config_valid = json.load(config_file)
    
            try:
                ID = importlib.import_module('import_' + config_valid['Logger Type'])            	
                #ID = importlib.import_module('import_' + config_valid['Logger Type'], package = 'none')
                V_DATA = ID.import_data(config_valid['Data Folder'],dates)
    
                V_DATA.index.name ='Timestamp'
                data_imported += 1
    
                # Just extract wind speed and direction columns
                needed_columns = [] 
    
                # Condition wind speed data by making all the bad data nan
                columns = [col for col in V_DATA.columns if 'Anem' in col and 'flagged' in col]
                for flag_col in columns:
                    data_col = flag_col.replace('_flagged',"")
                    V_DATA.loc[V_DATA[flag_col] < 8, data_col] = None
                    needed_columns.append(data_col)
    
                # Condition wind direction data by making all the bad data nan
                columns = [col for col in V_DATA.columns if 'Vane' in col and 'flagged' in col]
                for flag_col in columns:
                    data_col = flag_col.replace('_flagged',"")
                    V_DATA.loc[V_DATA[flag_col] < 8, data_col] = None
                    needed_columns.append(data_col)
    
                # Keep only needed columns
                V_DATA = V_DATA[needed_columns]
                    
            except Exception as e: 
                print('ERROR: Data importation error \n')
                print(str(e))
    
    
            # IMPORT FLAG INFORMATION
            with open('flag_information.json') as config_file:    
                flag_info = json.load(config_file)
    
            print('')
    
    
    
        # PREPARE REPORT, ANALYSE AND PLOT DATA
        if data_imported == 3:
    
            print('')
            print('CALCULATING AIR DENSITY')
            print('----------------------------------------------------------------')
    
            temp_flag = 0

            p0=101.325 # kPa - one atmosphere
            Rd=287.04  # J/Kg/K - universal gas constant
            g=9.80665  # m/s^2 - gravity
            thor=0.0065 #K/m - lapse rate

            mean_airTemp = float('nan')


            # Calculate Mean air temp
            if 'AT_2' in DATA.columns:
                mean_airTemp = DATA['AT_2'].mean()
                temp_flag = 1
    
            print(mean_airTemp)
            if np.isnan(mean_airTemp):
                SeaLevelTemp=15.00  # use prescribed sea level temperature but ask to modify
                changeSeaTemp=input('Assuming sea level temp is ' + "%.1f" % SeaLevelTemp + ' degrees celcius, change? (y/n)')
                if changeSeaTemp=='y':
                    SeaLevelTemp = float(input("Enter sea level temp: "))
                mean_airTemp=SeaLevelTemp-(thor*config_LIDAR['Elevation']) # calculate temperature at site
            print('Mean External Temp ' + str(mean_airTemp))
    

            # Calculate air density
            Psite=p0*np.power(((mean_airTemp+273.15)/(mean_airTemp+273.15+(thor*config_LIDAR['Elevation']))),(g/(Rd*thor)))
            AirDensity=1000*Psite/(Rd*(273.15 + mean_airTemp))
            print('Air Density: ' + "%.3f" %AirDensity)
            print('')
    
            # Write to table
            f=open(os.getcwd() + '/' +'latex_AirDensity.tex','w')
            if temp_flag == 1:
                f.write(r"* Air density calculated using average air temperature at site: " +"%.2f" % AirDensity + " kg m$^{-3}$ *"),f.write("\n")
            else:
                f.write(r"* Air density calculated using site elevation, standard lapse rate and a sea level temperature of " +"%.2f" % SeaLevelTemp + "$^{o}$C: " +"%.2f" % AirDensity + " kg m$^{-3}$ *"),f.write("\n")
            f.close()
            
    
    
            print('')
            print('CALCULATING STATISTICS')
            print('----------------------------------------------------------------')
    
            lidar_stats = pl.stats(os.getcwd(), DATA, 'DATA_L_stats.txt', flag_info, AirDensity)
            met_stats = pl.stats(os.getcwd(), M_DATA, 'DATA_M_stats.txt', flag_info, AirDensity)
    
            # AIR PRESSURE
            head1=['RR', 'ERR', 'Avg', 'Std', 'Max', 'Min']
            head2=['(\%)','(\%)','(kPa)','(kPa)','(kPa)','(kPa)']
            lw.write_stats_table(os.getcwd(), 'air_press', head1, head2, 'AP_2', lidar_stats)
    
            # AIR TEMPERATURE
            head1=['RR', 'ERR', 'Avg', 'Std', 'Max', 'Min']
            head2=['(\%)','(\%)','($^{o}$C)','($^{o}$C)','($^{o}$C)','($^{o}$C)']
            lw.write_stats_table(os.getcwd(), 'air_temp', head1, head2, 'AT_2', lidar_stats)
    
            # RELATIVE HUMIDITY
            head1=['RR', 'ERR', 'Avg', 'Std', 'Max', 'Min']
            head2=['(\%)','(\%)','(\%)','(\%)','(\%)','(\%)' ]
            lw.write_stats_table(os.getcwd(), 'rel_hum', head1, head2, 'RH_2', lidar_stats)
    
            # WIND SPEED
            head1=['Height' , 'RR', 'ERR', 'ARR', 'Avg', 'Std', 'Max', 'Min', 'Gust', 'Avg TI', 'IEC 15', 'WPD']
            head2=['(m)', '(\%)', '(\%)', '(\%)', '(m$s^{-1}$)', '(m$s^{-1}$)', '(m$s^{-1}$)', '(m$s^{-1}$)', '(m$s^{-1}$)', '(\%)','(\%)', '(W m$^{-2}$)']
            lw.write_stats_table(os.getcwd(), 'wind_spd', head1, head2, 'm Wind Speed (m/s)', lidar_stats)
    
            # WIND VANES
            head1=['Height', 'RR', 'ERR', 'ARR']
            head2=['(m)','(\%)', '(\%)', '(\%)']
            lw.write_stats_table(os.getcwd(), 'wind_dir', head1, head2, 'm Wind Direction (', lidar_stats)
  



            if run_met == 'y':    
                # SOLAR RADIATION
                head1=['RR', 'ERR', 'Avg', 'Tot', 'Max', 'Min']
                head2=['(\%)','(\%)','(Wm$^{-2}$)','(kWm$^{-2}$)','(Wm$^{-2}$)','(Wm$^{-2}$)']
                lw.write_stats_table(os.getcwd(), 'sol_rad', head1, head2, 'SR_2', met_stats)
        
                # PRECIPITATION
                head1=['RR', 'ERR', 'Avg', 'Tot', 'Max', 'Min']
                head2=['(\%)','(\%)','(mm)','(mm)','(mm)', '(mm)']
                lw.write_stats_table(os.getcwd(), 'precip', head1, head2, 'PR_2', met_stats)
                
    
    
    
            print('')
            print('WRITING MISSING DATA INFORMATION')
            print('----------------------------------------------------------------')
    
            
            # AIR PRESSURE
            lw.write_missing_data_general(os.getcwd(), 'Air_Press', pd.DataFrame(DATA, columns = [col for col in DATA.columns if 'AP_2' in col]), 
                'Air Pressure', 'kPa', config_met["Air Pressure"]["Serial Number"], flag_info)
    
            # AIR TEMPERATURE 
            lw.write_missing_data_general(os.getcwd(), 'Air_Temp', pd.DataFrame(DATA, columns = [col for col in DATA.columns if 'AT_2' in col]), 
                'Air Temperature', '$^o$C', config_met["Air Temperature"]["Serial Number"], flag_info)
            #lw.write_missing_data_general(os.getcwd(), 'Air_Temp', pd.DataFrame(DATA, columns = [col for col in DATA.columns if 'Ext Temp (°C)' in col]), 
                #'Air Temperature', '$^o$C', config_met["Air Temperature"]["Serial Number"], flag_info)   
            # RELATIVE HUMIDITY
            lw.write_missing_data_general(os.getcwd(), 'Rel_Hum', pd.DataFrame(DATA, columns = [col for col in DATA.columns if 'RH_2' in col]), 
                'Relative Humidity', '\%', config_met["Relative Humidity"]["Serial Number"], flag_info)

            if run_met == 'y':
                # SOLAR RADIATION
                lw.write_missing_data_general(os.getcwd(), 'Solar_Rad', pd.DataFrame(M_DATA, columns = [col for col in M_DATA.columns if 'SR_2' in col]), 
                    'Solar Radiation', 'W m$^{-2}$', config_met["Solar Radiation"]["Serial Number"], flag_info)
        
                # PRECIPITATION
                lw.write_missing_data_general(os.getcwd(), 'Precipitation', pd.DataFrame(M_DATA, columns = [col for col in M_DATA.columns if 'PR_2' in col]), 
                    'Precipitation', 'mm', config_met["Precipitation"]["Serial Number"], flag_info)
    
            # LIDAR DATA
            lw.write_missing_data_lidar(os.getcwd(),pd.DataFrame(DATA, columns=[col for col in DATA.columns if 'm Wind Speed (m/s)' in col]), flag_info)
    
    
    
    
            print('')
            print('CONSTRUCTING REPORT')
            print('----------------------------------------------------------------')
    
            # Front Page
            front_page_details = []
            #input('')
            front_page_details.append(input("Enter report number: "))
            front_page_details.append(input("Enter report author's name: "))
            front_page_details.append(input("Enter report editor's name: "))
            page_title=['Wind Data Analysis','and','Verification Report']
            lw.frontPage(os.getcwd(),config_LIDAR,front_page_details,page_title)
            print('')
    
            # Page Headers
            page_header_text = input("Enter report page header: ")
            lw.pageHeader(os.getcwd(),page_header_text)
            print('')
    
            # Site Details Page
            lw.siteDetails(os.getcwd(),config_LIDAR, config_met)
    
            # Get turbine hub height and blade details
            hub_height = int(input("Enter proposed hub height: "))
            blade_lower = int(input("Enter proposed blade lower height: "))
            blade_upper = int(input("Enter proposed blade upper height: "))

            hub_heights = [0,0,0]
            hub_heights[0] = blade_lower
            hub_heights[1] = hub_height
            hub_heights[2] = blade_upper

            f=open(os.getcwd() + '/' +'latex_blade_Bottom.tex','w')
            f.write(str(hub_heights[0]))
            f.write("\n")
            f.close()
    
            g=open(os.getcwd() + '/' +'latex_blade_Hub.tex','w')
            g.write(str(hub_heights[1]))
            g.write("\n")
            g.close()
    
            h=open(os.getcwd() + '/' +'latex_blade_Top.tex','w')
            h.write(str(hub_heights[2]))
            h.write("\n")
            h.close()   
    
    
    
            print('')
            print('PLOTTING METEOROLOGICAL DATA')
            print('----------------------------------------------------------------')
    
            # DRAW FLAG INFO
            pl.timeSeriesKey(os.getcwd(),flag_info)

            if monthly_or_period == 'm':
            	y_tick_vals = [0,25,50,75,100,125,150]
            else:
            	y_tick_vals = [0,100,200,300,400,500]
    

            if run_met == 'y':
                # RAINFALL
                kwargs = {'chartType': 'bar',
                            'yTickValues2':  y_tick_vals,
                            'barWidth': 0.005,
                            'figSize':(40,12),
                            'plotLegend':0,
                            'plotFlags':1,
                            'bdrWths':[1.05,0,0.05,0.98]}
                pl.plot_timeseries_line(os.getcwd(),
                pd.DataFrame(M_DATA, columns = [col for col in M_DATA.columns if 'PR_2' in col]),
                'Rain',np.arange(0,4.8,0.4),'Rainfall (mm)', flag_info, **kwargs)
    
            # AIR TEMPERATURE
            pl.plot_timeseries_line(os.getcwd(),
            pd.DataFrame(DATA, columns = [col for col in DATA.columns if 'AT_2' in col]),
            'Air_Temperature',np.arange(-10,50,5),'Air Temperature ($^{o}$C)', flag_info)
    
            # AIR PRESSURE
            pl.plot_timeseries_line(os.getcwd(),
            pd.DataFrame(DATA, columns = [col for col in DATA.columns if 'AP_2' in col]),
            'Air_Pressure',np.arange(85,110,5),'Air Pressure (kPa)', flag_info)
    
            # RELATIVE HUMIDITY
            pl.plot_timeseries_line(os.getcwd(),
            pd.DataFrame(DATA, columns = [col for col in DATA.columns if 'RH_2' in col]),
            'Relative_Humidity',np.arange(0,110,10),'Relative Humidity (%)', flag_info)

            if run_met == 'y':
                # SOLAR RADIATION
                pl.plot_timeseries_line(os.getcwd(),
                pd.DataFrame(M_DATA, columns = [col for col in M_DATA.columns if 'SR_2' in col]),
                'Solar_Radiation',np.arange(0,1500,250),'Solar Radiation (W/m$^{2}$)', flag_info)
    
            # WIND SPEED
            pl.plot_timeseries_line(os.getcwd(),
            pd.DataFrame(DATA, columns = [col for col in DATA.columns if 'm Wind Speed (m/s)' in col]),
            'Wind_Speed',np.arange(0,40,5),'Wind Speed (ms$^{-1}$)', flag_info)
    
            # WIND DIRECTION
            kwargs = {'markerSize': 10,
                    'markerType' : '.',
                    'lineStyle' : 'none',
                    'lineWidth':0,
                    'figSize':(40,12),
                    'bdrWths':[1.05,0,0.05,0.98]}    
            pl.plot_timeseries_line(os.getcwd(),
            pd.DataFrame(DATA, columns = [col for col in DATA.columns if 'm Wind Direction (' in col]),
            'Wind_Direction',np.arange(0,450,90),'Wind Direction ($^{o}$)', flag_info, **kwargs)
    
    
    
            
    
            print('')
            print('PLOTTING WIND RESOURCE DATA')
            print('----------------------------------------------------------------')
    
    
            # DATA AVAILABILITY
            #pl.plot_data_availability(os.getcwd(),
            #pd.DataFrame(DATA, columns=[col for col in DATA.columns if 'm Data Availability (%)' in col]),
            #[40,60,80,90,100,110,120,140,160,180,200],'Data Availability (%)',(0,100,20))          
    
            # WIND SPEED FOR PERIOD
            #pl.plot_timeseries_contour(os.getcwd(),'WS',
            #    pd.DataFrame(DATA, columns=[col for col in DATA.columns if 'm Wind Speed (m/s)' in col]),
            #    [40,60,80,90,100,110,120,140,160,180,200],'Wind Speed (ms$^{-1}$)',[0.0,25.0,2])
    
            # WIND DIRECTION FOR PERIOD
            #pl.plot_timeseries_contour(os.getcwd(),'WD',
            #    pd.DataFrame(DATA, columns=[col for col in DATA.columns if 'm Wind Direction (' in col]),
            #    [40,60,80,90,100,110,120,140,160,180,200],'Wind Direction ($^{o}$)',[0.0,375,15])
    

            if run_validation == 'y':
                # WIND SPEED VALIDATION AGAINST TOWER
                pl.plot_data_validation(os.getcwd(),
                    pd.DataFrame(DATA, columns=[col for col in DATA.columns if 'm Wind Speed (m/s)' in col and not '_flagged' in col]),
                    pd.DataFrame(V_DATA, columns=[col for col in V_DATA.columns if 'Anem' in col]),'WindSpeed',30,2,6,0.4)
    
                # WIND DIRECTION VALIDATION AGAINST TOWER
                pl.plot_data_validation(os.getcwd(),
                    pd.DataFrame(DATA, columns=[col for col in DATA.columns if 'm Wind Direction (' in col and not '_flagged' in col]),
                    pd.DataFrame(V_DATA, columns =[col for col in V_DATA.columns if 'Vane' in col]),'WindDirection',360,10,90,0.25)
    
            # DAILY WIND SPEED
            daily=DATA.groupby(DATA.index.hour).mean()
            pl.plot_WS_daily(os.getcwd(),'',
                pd.DataFrame(daily, columns=[col for col in daily.columns if 'm Wind Speed (m/s)' in col and not '_flagged' in col]),
                config_LIDAR['measurement heights'])
    
            # DAILY WIND DIRECTION
            c_levels = np.arange(0, 35, 1)
            f_levels = np.arange(0, 35, 5)
            
            if monthly_or_period == 'p':
                c_levels = np.arange(0, 15, 1)
                f_levels = np.arange(0, 15, 2)

            pl.plot_WD_daily(os.getcwd(),'',
                pd.DataFrame(DATA, columns=[col for col in DATA.columns if str(hub_heights[1])+'m Wind Direction (' in col and not '_flagged' in col]), f_levels, c_levels)
    
            # WIND SHEAR BY DIRECTION
            pl.plot_wind_shear(os.getcwd(),DATA,hub_heights,config_LIDAR['measurement heights'])
    
            # WIND CORRELATION BY DIRECTION
            pl.plot_wind_correlation_by_direction(os.getcwd(),DATA,hub_heights)


            if monthly_or_period == 'p':
                # MONTHLY AVERAGE WIND SPEED BY HEIGHT 
                pl.plot_WS_monthly(os.getcwd(),'',
                    pd.DataFrame(DATA, columns=[col for col in DATA.columns if 'm Wind Speed (m/s)' in col and not '_flagged' in col]),
                    config_LIDAR['measurement heights'])
     
    
    
    
            for c001 in range(0,3):
    
                WS_col = [col for col in DATA.columns if str(hub_heights[c001])+ 'm Wind Speed (' in col and not '_flagged' in col]
                WSD_col = [col for col in DATA.columns if str(hub_heights[c001])+ 'm Wind Speed Dispersion (' in col and not '_flagged' in col]
                WD_col = [col for col in DATA.columns if str(hub_heights[c001])+ 'm Wind Direction (' in col and not '_flagged' in col]
                    
                WS = DATA[WS_col[0]]
                WD = DATA[WD_col[0]]
    
                # WIND ROSES
                pl.plot_wind_rose(os.getcwd(),WS,WD,(0, 20, 5),str(c001),'Wind Speed (m s$^{-1}$)')
    
                # WEIBULL PLOTS
                pl.plot_weibull_distribution(os.getcwd(),WS,(0.,30.,1.),str(c001))
    
                # TURBULENCE INTENSITY
                cols=[]
                cols.append(WS_col[0])
                cols.append(WSD_col[0])
                pl.plot_turbulence_intensity(os.getcwd(),DATA[cols].values,str(c001))
    
    
    if run_full_report == 'n':
        report_folder = input(" Enter the name of the report folder that needs re-commenting: ")

        files_to_move = os.listdir(os.getcwd() + '\\' + report_folder + '\\')
        print(files_to_move)
        for f in files_to_move:
            try: 
                shutil.move(os.getcwd() + '\\' + report_folder +'\\' + f, os.getcwd())
            except Exception as e:
                print('cannot move ' + f)
                print(str(e))





    lw.importComments(os.getcwd())


    # CREATE PDF

    # File name
    pdf_file_name = input("Enter pdf file name: ")

    suffix = ''
    if ((run_met == 'n') and (run_validation == 'n')):
        suffix = '_noValid_noMet'
    
    # Create pdf
    if monthly_or_period == 'm':
        os.system('pdflatex ReportTemplate_LIDAR_monthly' + suffix + '.tex')
    else:
        os.system('pdflatex ReportTemplate_LIDAR_period' + suffix + '.tex')

    # Create folder and copy/move files to it
    os.system('mkdir '+ pdf_file_name)

    if monthly_or_period == 'm':
        shutil.copyfile('P:/SOFTWARE/Reporting Tools/lidar-reporting/' + 'ReportTemplate_LIDAR_monthly' + suffix + '.pdf' , './' + pdf_file_name + '/' + pdf_file_name + '.pdf') 
    else:
        shutil.copyfile('P:/SOFTWARE/Reporting Tools/lidar-reporting/' + 'ReportTemplate_LIDAR_period' + suffix + '.pdf' , './' + pdf_file_name + '/' + pdf_file_name + '.pdf') 




    os.system('move FIG* ./' + pdf_file_name + '/')
    os.system('move latex_* ./' + pdf_file_name + '/')
    shutil.copyfile('P:\SOFTWARE\Reporting Tools\lidar-reporting\BackgroundEven.png' , './' + pdf_file_name + '/' + 'BackgroundEven.png')    
    shutil.copyfile('P:\SOFTWARE\Reporting Tools\lidar-reporting\BackgroundOdd.png'  , './' + pdf_file_name + '/' + 'BackgroundOdd.png')
    shutil.copyfile('P:\SOFTWARE\Reporting Tools\lidar-reporting\BackgroundFront.png', './' + pdf_file_name + '/' + 'BackgroundFront.png')



import_data_and_plot()













        
