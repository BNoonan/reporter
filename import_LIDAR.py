import os
import time
import re
import pandas as pd
import csv

import datetime as dt
from tkinter import *
from tkinter import filedialog

def import_data(folder,dates):
    # Check headers the same as last time - if new site create header file

    # 
    print('IMPORTING LIDAR DATA')
    data_files_to_import=[]

    
    # SEARCH FOLDER FOR DATA FILES (with .sta end)
    original_folder = os.getcwd()
    os.chdir(folder)


    # CONSTRUCT START AND END TIMES FOR TESTING FILES
    date_start = time.strptime(str(dates[2]) + '/' + str(dates[1]) + '/' + str(dates[0]), "%d/%m/%Y")
    date_end = time.strptime(str(dates[5]) + '/' + str(dates[4]) + '/' + str(dates[3]), "%d/%m/%Y")

    master = Tk()
    master.withdraw() #hiding tkinter window for some reason?
    path = filedialog.askopenfilename(initialdir = folder, title="Select data file", filetypes=[("csv file","*.csv"),("All files",".*")])
    data_files_to_import.append(os.path.basename(path))
    
    # TODO DELETE
    print('IMPORTING: ' + str(data_files_to_import[0]))

    # READ FILE NAMES
    '''data_files_to_import=[]
    for a_file in os.listdir(os.getcwd()):
        if a_file.endswith('.sta'):
            t01 = a_file.split('_')
            file_date = time.strptime(str(t01[3]) + '/' + str(t01[2]) + '/' + str(t01[1]), "%d/%m/%Y")

            
            # TEST TO SEE WHICH FILES IN TIME PERIOD AND STORE THOSE THAT ARE
            if file_date <= date_end and file_date >= date_start:
                data_files_to_import.append(a_file)
    '''

    '''
    READ HEADER FROM FIRST DATA FILE
    --------------------------------------------------------------------------------------
        Extract information from header about site location, timezone and measurement heights.
    '''
    try:
                
        '''
        IMPORT DATA AS INDIVIDUAL PANDAS DATAFRAMES AND CONCATENATE
        --------------------------------------------------------------------------------------
        '''
        dataframe = []
        for a_file in data_files_to_import:
            # Read data into pandas array and construct index from Timestamp column
            #df = pd.read_csv(a_file, header = int(info_HeaderSize), sep='\t', encoding = "ISO-8859-1")
            #df = pd.read_csv(a_file, header = int(info_HeaderSize), sep=',', encoding = "ISO-8859-1")   

            #try:
            dateparse = lambda x: pd.datetime.strptime(x, '%Y/%m/%d %H:%M')
            #except:
            #dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M')                
#           df = pd.read_csv(a_file, header = 0,sep=',', encoding = "ISO-8859-1", parse_dates=['TimeStamp'], date_parser=dateparse, engine = 'python')            
            df = pd.read_csv(a_file, header = 0,sep=',', encoding = "UTF-8", parse_dates=['TimeStamp'], date_parser=dateparse, engine = 'python')            

            #dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M')
            #df = pd.read_csv(a_file, header = int(info_HeaderSize),sep=',', encoding = "ISO-8859-1", parse_dates=['Timestamp (end of interval)'], date_parser=dateparse, engine = 'python')

            df.index = pd.DatetimeIndex(df["TimeStamp"])
            del df['TimeStamp']
            dataframe.append(df) # store dataframes in list


        dataframe = pd.concat(dataframe)
        dataframe.sort_index(inplace=True)

        DATA = dataframe

    except Exception as e: print (str(e))
    #except:
        #print('No files found for specified dates')


    # RETURN PROGRAM TO ORIGINAL FOLDER
    os.chdir(original_folder)

    return(DATA)