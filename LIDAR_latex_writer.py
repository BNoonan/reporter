# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from tkinter import *
from tkinter import filedialog




def frontPage(folder,siteDetails,frontPageDetails,pageTitle):
    ''' 


    '''

    f=open(folder + '/' +'latex_ReportFrontPage.tex','w')
    f.write(r'\begin{center}'), f.write("\n")
    f.write(r'\vspace*{100px}'), f.write("\n")
    for line in pageTitle:
        f.write(r'{\huge \bfseries ' + line +  r'\\[0.4cm]}')
    f.write("\n")
    f.write(r'\vfil'), f.write("\n")
    f.write(r'{ \huge \bfseries {')
    f.write(siteDetails['Project']) # PROJECT
    f.write(r'} \\[0.4cm] }'), f.write("\n")
    f.write(r'{ \huge \bfseries {')
    f.write(siteDetails['Site Name']) # SITE NAME
    f.write(r'} \\[0.4cm] }'), f.write("\n")
    f.write(r'{ \huge \bfseries {')
    f.write(frontPageDetails[0])  # REPORT NUMBER
    f.write(r'} \\[0.4cm] }'), f.write("\n")
    f.write(r'\vfil'), f.write("\n")
    f.write(r'\begin{minipage}{0.25\textwidth}'), f.write("\n")
    f.write(r'\begin{flushright} \large '), f.write("\n")
    f.write(r'\bf Client: \\'), f.write("\n")
    f.write(r'\bf Client Contact: \\'), f.write("\n")
    f.write(r'\bf Report Prepared: \\'), f.write("\n")
    f.write(r'\bf Report Author: \\'), f.write("\n")
    f.write(r'\bf Report Editor: '), f.write("\n")
    f.write(r'\end{flushright}'), f.write("\n")
    f.write(r'\end{minipage}'), f.write("\n")
    f.write(r'\hspace{0.8 cm}'), f.write("\n")
    f.write(r'\begin{minipage}{0.6\textwidth}'), f.write("\n")
    f.write(r'\begin{flushleft} \large '), f.write("\n")
    f.write(siteDetails['Client'])  # CLIENT
    f.write(r' \hfill \\'), f.write("\n")
    f.write(siteDetails['Client Contact'])  # CLIENT CONTACT
    f.write(r' \hfill \\'), f.write("\n")
    f.write(r'\today') # DATE REPORT PREPARED
    f.write(r' \hfill \\'), f.write("\n")
    f.write(frontPageDetails[1]) # REPORT AUTHOR
    f.write(r' \hfill \\'), f.write("\n")
    f.write(frontPageDetails[2]) # REPORT EDITOR
    f.write(r' \hfill'), f.write("\n")
    f.write(r'\end{flushleft}') , f.write("\n")
    f.write(r'\end{minipage}'), f.write("\n")
    f.write(r'\end{center}'), f.write("\n")
    f.close()



def pageHeader(folder,pageHeaderText):
    ''' 


    '''

    f=open(folder + '/' +'latex_PageHeader.tex','w')
    f.write(pageHeaderText), f.write('\n')
    f.close()



def siteDetails(folder,LIDAR_details, met_details):
    ''' 


    '''

    f=open(folder + '/' + 'latex_siteDetails.tex','w')
    f.write(r"\begin{tabular}{p{4cm} p{8cm}}"), f.write("\n")

    # LIDAR DETAILS
    f.write(r"\multicolumn{2}{l}{\small{\bf{LiDAR DETAILS}}} \\"), f.write("\n")
    f.write(r"\\"), f.write("\n")
    details = LIDAR_details['LIDAR Details']
    for c002 in range(1, len(details)+1):
        temp = details[str(c002)]
        detail = temp.split(':')
        f.write(r"\bf{")
        f.write(detail[0])
        f.write("} & {")
        f.write(detail[1])
        f.write("}")
        f.write(r"\\"), f.write("\n")
    f.write(r"\\"), f.write("\n")
    f.write(r"\\"), f.write("\n")

    # VALIDATION DETAILS
    f.write(r"\multicolumn{2}{l}{\small{\bf{VALIDATION DETAILS}}} \\"), f.write("\n")
    f.write(r"\\"), f.write("\n")
    details = LIDAR_details['VALID Details']
    for c002 in range(1, len(details)+1):
        temp = details[str(c002)]
        detail = temp.split(':')
        f.write(r"\bf{")
        f.write(detail[0])
        f.write("} & {")
        f.write(detail[1])
        f.write("}")
        f.write(r"\\"), f.write("\n")
    f.write(r"\end{tabular}"), f.write("\n")
    f.write(r"\vfill"), f.write("\n")

    # METEOROLOGICAL DETAILS    
    f.write(r"\begin{tabular}{p{4cm} p{2cm} p{2cm} p{2cm} p{2cm}}"), f.write("\n")
    f.write(r"\multicolumn{5}{l}{\small{\bf{METEOROLOGICAL DETAILS}}} \\"), f.write("\n")
    f.write(r"\\"), f.write("\n")
    sequence = ["Air Pressure","Air Temperature","Relative Humidity","Solar Radiation","Precipitation"]
    f.write(r"\bf{Sensor} & \bf{Model} & \bf{Logger} & \bf{Serial Number} & \bf{Install Date}")
    f.write(r"\\"), f.write("\n")
    for c002 in range(0, len(sequence)):
        temp = met_details[sequence[c002]]
        f.write(r"\bf{")
        f.write(sequence[c002])
        f.write("} & {")
        f.write(temp["Model"])
        f.write("} & {")
        f.write(temp["Logger"])
        f.write("} & {")
        f.write(temp["Serial Number"])
        f.write("} & {")
        f.write(temp["Install Date"])
        f.write("}")
        f.write(r"\\"), f.write("\n")
    f.write(r"\end{tabular}"), f.write("\n")
    f.write(r"\vfill"), f.write("\n")

    f.close()





def write_missing_data_lidar(folder, df, flag_info):
    '''



    '''

    good_flag = flag_info['good flag']
    flag_messages = flag_info['flag messages']


    # PLOT DATA
    # Seperate data and flag column names
    data_cols = []
    flag_cols = []
    for col in df.columns:
        if 'flagged' in col:
            flag_cols.append(col)

        else:
            data_cols.append(col)

    # open file
    f=open(folder + '/latex_availability_lidar.tex','w')
    f.write(r'\begin{tabbing}'), f.write('\n')
    # set up tab for spacing between flag and time period
    f.write(r'{\hskip 30em} \=  \\'), f.write('\n')

    # WRITE HEADER (eg. Air Temperature Sensors, Anemometers, Missing Data)
    f.write(r'\\'), f.write('\n')
    f.write(r'\\'), f.write('\n')
    f.write(r'\small{\bf{LiDAR}}'), f.write('\n')
    f.write(r'\\'), f.write('\n')


    # Go through flag columns
    column_counter = 0
    for col in flag_cols:
        f.write(r'\\'), f.write('\n')
        f.write(r'\underline{\bf{' + col.replace("m Wind Speed (m/s)_flagged", "m Height") + '}}'), f.write(r'\\'), f.write('\n')

        some_missing = 0
        write_trigger = 0
        current_flag = 0
        write = 0

        flag_data = df[col]
        for counter in range(0, len(flag_data) - 1):
            flag = flag_data[counter]
            next_flag = flag_data[counter + 1]


            # If current flag not a good flag
            if flag != good_flag:
                if write_trigger == 0: 
                    some_missing = 1 # flag to say there is missing data
                    write_trigger = 1 # says something needs to be written
                    current_flag = flag # which flag is active
                    start_place = counter # where the flag is in the data

            # if bad flags found triggered......
            if write_trigger == 1:

                # If next flag not the same as current - write flag
                if next_flag != current_flag:
                    write = 1
                    end_place = counter

                # if next flag is at the end of the data set and is still bad - write flag
                if counter + 1 == len(flag_data) - 1 :
                    if next_flag == current_flag: 
                        write = 1
                        end_place = counter + 1

            # if single flag at end of flag data - write flag
            if write_trigger == 0:
                if next_flag != good_flag and counter + 1 == len(flag_data) - 1:
                    write = 1
                    start_place = counter + 1
                    end_place = counter + 1
                    current_flag = flag_data[counter + 1]

            if write == 1:

                #f.write(flag_messages[str(current_flag)] + ': ' + '\>')
                f.write(flag_messages[str(int(current_flag))] + ': ' + '\>')                
                if start_place == end_place:
                    f.write(str(df.index[start_place]))
                else:
                    f.write(str(df.index[start_place]) + '    ' + r'$\to$' + '    ' + str(df.index[end_place]))
                f.write(r'\\'), f.write('\n')
                f.write('\n')

                # Reset write trigger
                start_place = 0
                write_trigger = 0
                end_place = 0
                write = 0



        # If no flags
        if some_missing == 0:            
            f.write('No apparent issues or missing data'), f.write(r'\\'), f.write('\n')

        column_counter += 1

    f.write(r'\\'), f.write('\n')
    f.write(r'\begin{minipage}{0.9\textwidth}'), f.write('\n')
    f.write(r'\scriptsize{\bf{Comments: }\textnormal{\input{ latex_Comment_availability_lidar.txt }}}'), f.write('\n')
    f.write(r'\end{minipage}'), f.write('\n')
    f.write(r'\\'), f.write('\n')
    f.write(r'\end{tabbing}'), f.write('\n')
    f.close()



def write_missing_data_general(folder, filename, df, header, units, serial_number, flag_info):
    '''



    '''

    good_flag = flag_info['good flag']
    flag_messages = flag_info['flag messages']


    # PLOT DATA
    # Seperate data and flag column names
    data_cols = []
    flag_cols = []
    for col in df.columns:
        if 'flagged' in col:
            flag_cols.append(col)

        else:
            data_cols.append(col)

    # open file
    f=open(folder + '/latex_availability_' + filename + '.tex','w')
    f.write(r'\begin{tabbing}'), f.write('\n')
    # set up tab for spacing between flag and time period
    f.write(r'{\hskip 30em} \=  \\'), f.write('\n')

    # WRITE HEADER (eg. Air Temperature Sensors, Anemometers, Missing Data)
    f.write(r'\\'), f.write('\n')
    f.write(r'\\'), f.write('\n')
    f.write(r'\small{\bf{' + header + '}}'), f.write('\n')
    f.write(r'\\'), f.write('\n')

    # Go through flag columns
    column_counter = 0
    for col in flag_cols:
        f.write(r'\\'), f.write('\n')
        f.write(r'\underline{\bf{' + serial_number + '}}'), f.write(r'\\'), f.write('\n')

         
        some_missing = 0
        write_trigger = 0
        current_flag = 0
        write = 0

        flag_data = df[col]
        for counter in range(0, len(flag_data) - 1):
            flag = flag_data[counter]
            next_flag = flag_data[counter + 1]

            # If current flag not a good flag
            if flag != good_flag:
                if write_trigger == 0: 
                    some_missing = 1 # flag to say there is missing data
                    write_trigger = 1 # says something needs to be written
                    current_flag = flag # which flag is active
                    start_place = counter # where the flag is in the data

            # if bad flags found triggered......
            if write_trigger == 1:

                # If next flag not the same as current - write flag
                if next_flag != current_flag:
                    write = 1
                    end_place = counter

                # if next flag is at the end of the data set and is still bad - write flag
                if counter + 1 == len(flag_data) - 1 :
                    if next_flag == current_flag: 
                        write = 1
                        end_place = counter + 1

            # if single flag at end of flag data - write flag
            if write_trigger == 0:
                if next_flag != good_flag and counter + 1 == len(flag_data) - 1:
                    write = 1
                    start_place = counter + 1
                    end_place = counter + 1
                    current_flag = flag_data[counter + 1]

            if write == 1:
                df_sensor = pd.DataFrame(df, columns=[col for col in df.columns if data_cols[column_counter] in col and not '_flagged' in col])
                val = (df_sensor.values[start_place][column_counter])


                # if value is 'nan'
                if np.isnan(val):
                    f.write(flag_messages[str(int(current_flag))] + ':' + '\>')
                else:
                    strVal = str('%.2f' % val)
                    f.write(flag_messages[str(int(current_flag))] + ': (' + strVal + ' ' + units + ') ' + '\>')

                if start_place == end_place:
                    f.write(str(df.index[start_place]))
                else:
                    f.write(str(df.index[start_place]) + '    ' + r'$\to$' + '    ' + str(df.index[end_place]))
                f.write(r'\\'), f.write('\n')
                f.write('\n')

                # Reset write trigger
                start_place = 0
                write_trigger = 0
                end_place = 0
                write = 0
            
        # If no flags
        if some_missing == 0:            
            f.write('No apparent issues or missing data'), f.write(r'\\'), f.write('\n')

        column_counter += 1

    f.write(r'\\'), f.write('\n')
    f.write(r'\begin{minipage}{0.9\textwidth}'), f.write('\n')
    f.write(r'\scriptsize{\bf{Comments: }\textnormal{\input{ latex_Comment_availability_' + filename + '.txt }}}'), f.write('\n')
    f.write(r'\end{minipage}'), f.write('\n')
    f.write(r'\\'), f.write('\n')
    f.write(r'\end{tabbing}'), f.write('\n')
    f.close()











def write_stats_table(folder,filename,header1,header2,sensor_details,stats_table):
    '''


    '''

    # Open file
    f=open(folder + '/latex_stats_' + filename + '.tex','w')

    # prepare table format and headers
    columnFormat='{'   # define columns
    for cnt01 in range(0,len(header1)):
        columnFormat=columnFormat + ' c '
    columnFormat=columnFormat+'}'

    # write to latex file
    f.write(r'\begin{tabular}')
    f.write(columnFormat), f.write('\n')

    # write column headers - header 1
    columnHeader=''
    for cnt01 in range(0,len(header1)):
        columnHeader=columnHeader + header1[cnt01]
        if cnt01 < len(header1)-1:
            columnHeader=columnHeader+ ' & '
    f.write(columnHeader), f.write(r'\\'), f.write('\n')
    
    # write column headers - header 2
    columnHeader=''
    for cnt01 in range(0,len(header2)):
        columnHeader=columnHeader + header2[cnt01]
        if cnt01 < len(header2)-1:
            columnHeader=columnHeader+ ' & '
    f.write(columnHeader), f.write(r'\\'), f.write('\n')
    f.write('\hline'), f.write('\n')

    # write data to table
    column = [col for col in stats_table.columns if sensor_details in col and not '_flagged' in col]
    for c in column:

        c001 = 0
        for h in header1:

            if np.isnan(stats_table[c][h]):
                f.write(r'n/a')
            else:
                f.write('%.2f' % stats_table[c][h])

            if c001 < len(header1)-1:
                f.write(' & ')    

            c001 += 1

        f.write(r'\\'), f.write('\n')      

    f.write('\n')
    f.write('\n')
    f.write(r'\end{tabular}')
    f.close()




def importComments(folder):
    '''PURPOSE - import report comments'''

    ''' Open window to select comments file'''
    print('')
    print ("Importing report comments, select file")

    master = Tk()
    master.withdraw() #hiding tkinter window for some reason?

    path = filedialog.askopenfilename(initialdir = folder, title="Select Comment file", filetypes=[("text file","*.txt"),("All files",".*")])
    #path = filedialog.askopenfilename(title='Select Comment File', filetypes=[(fileType,fileExt),("All files","Comment_file.txt")])

    print(path)
    ''' Read in comments '''
    temp_1 = open(path, 'r')
    Comments=list(temp_1)
    #folder = dirname(path) # store folder name

    '''Store where section headers are so can divide comments file (look for ->, and extract file name between '* *'s )'''
    whereSections=[]
    whereFileNames=[0]
    cnt01=0
    for line in Comments:
        # Look for section headers
        if '->' in line:
            whereSections.append(cnt01)
        # look for file names
        if '**' in line:
            whereFileNames.append(cnt01)
        cnt01=cnt01+1
    whereSections.append(len(Comments)+1)

    ''' Write comments to individual files'''
    for cnt02 in range(1,len(whereSections)):
        Section_start=int(whereFileNames[cnt02]+1)
        Section_end=int(whereSections[cnt02]-1)

        fileName = Comments[whereFileNames[cnt02]].replace("**","")
        fileName2 = fileName.replace("\n","")
        print(folder+'/'+fileName2)
        f=open(folder + '/' + fileName2,'w')


        for cnt03 in range(Section_start,Section_end):
            line=Comments[cnt03]
            # replace %'s with \%'s for latex
            line = line.replace("%","\%")
            f.write(line)
        f.close()