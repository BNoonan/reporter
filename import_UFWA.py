import os
import time
import re
import pandas as pd

from tkinter import *
from tkinter import filedialog

def import_data(folder,dates):
    #TODO  Check headers the same as last time - if new site create header file

    # TODO DELETE
    print('IMPORTING VALIDATION DATA')
    data_files_to_import=[]

    # SEARCH FOLDER FOR DATA FILES WITH '.csv' SUFFIX
    original_folder = os.getcwd()
    os.chdir(folder)

    # CONSTRUCT START AND END TIMES FOR TESTING FILES
    date_start = time.strptime(str(dates[2]) + '/' + str(dates[1]) + '/' + str(dates[0]), "%d/%m/%Y")
    date_end = time.strptime(str(dates[5]) + '/' + str(dates[4]) + '/' + str(dates[3]), "%d/%m/%Y")

    '''
    # READ FILE NAMES
    data_files_to_import=[]
    for a_file in os.listdir(os.getcwd()):
        if a_file.endswith('.csv'):
        	data_files_to_import.append(a_file)

    print('IMPORTING: ' + str(data_files_to_import))

    '''

    master = Tk()
    master.withdraw() #hiding tkinter window for some reason?
    path = filedialog.askopenfilename(initialdir = folder, title="Select data file", filetypes=[("csv file","*.csv"),("All files",".*")])
    data_files_to_import.append(os.path.basename(path))
    
    # TODO DELETE
    print('IMPORTING: ' + str(data_files_to_import[0]))

    # OPEN SELECTED FILE(S)
    file = open(data_files_to_import[0], 'r', encoding='iso-8859-1')
    fileContent=list(file)

    c001=0
    # SEARCH FOR HEADER LINE NUMBER - DENOTED BY 'Date & Time Stamp'
    for line in fileContent:

        if 'TimeStamp' in line:
            header_line_number=c001

        # PANDAS IGNORES BLANK LINES SO DECREMENT COUNTER
        if line in ['\n', '\r\n']:
            c001 -= 1  
        c001 += 1

    print(header_line_number)
    # READ DATA INTO PANDAS ARRAY USING FOUND HEADER LINE NUMBER
    DATA = pd.read_csv(data_files_to_import[0], parse_dates=[0], dayfirst=True, 
        header = header_line_number, sep=',')


    # REASSIGN INDEX TO 'Timestamp' 
    DATA.index = pd.DatetimeIndex(DATA['TimeStamp'])
    del DATA['TimeStamp'] 
    DATA.index.name = 'Timestamp'

    # RETURN PROGRAM TO ORIGINAL FOLDER
    os.chdir(original_folder)

    return(DATA)
