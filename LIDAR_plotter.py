
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
import scipy as sci
from scipy import stats
from scipy.stats import exponweib as exw
from datetime import datetime
import re
import collections
import os
import math





def timeSeriesKey(folder,flag_info):
    ''' 


    '''

    colours = flag_info["flag colours"]
    messages = flag_info["flag messages"]

    #mpl.rc('font', **plot_font)
    plt.figure(figsize=(19.4,2.5))

    # plot colored rectangle for each flag
    c003=0
    yScale=2
    xScale=6
    for c002 in range(0, 2):
        for c001 in range(0, 5):
            rect = plt.Rectangle((c001*xScale,c002*yScale), 1, 1, facecolor = colours[str(c003)], edgecolor = 'black', linewidth = 3)
            plt.gca().add_patch(rect)
            plt.text(1.22+c001*xScale, 0.35+c002*yScale, messages[str(c003)], ha = 'left', rotation = 0, fontsize = 12)
            plt.xlim(-0.1,29)
            plt.ylim(-0.1,3.5)
            c003+=1
            # skips no communication
            if c003 == 2:
                c003 = 3
            plt.axis('off')
    plt.savefig(folder + '/' +'FIG_flagKey', bbox_inches='tight')
    plt.close()



def stats(folder, df, filename, flag_info, air_density):
    '''


    '''
    stats_table = pd.DataFrame(columns=df.columns, index = ['Height','Avg','Max','Min','Std','Sum','RR','ERR','ARR','Avg TI','IEC 15','WPD','Gust'])

    for col in df.columns:
        print(col)
        stats_table[col]['Height'] = 0
        stats_table[col]['Avg']  = df[col].mean()
        stats_table[col]['Max']  = df[col].max()
        stats_table[col]['Min']  = df[col].min()
        stats_table[col]['Std']  = df[col].std()
        stats_table[col]['Tot']  = df[col].sum()

        # Calculate recovery rate
        if col + '_flagged' in df.columns:

            stats_table[col]['RR']   = 100 * sum(df[col + '_flagged'] > flag_info["missing flag"]) / len(df[col + '_flagged'])
            stats_table[col]['ERR']  = 100 * sum(df[col + '_flagged'] >= flag_info["good flag"]) / len(df[col + '_flagged'])
            stats_table[col]['ARR']  = 100 * sum(df[col + '_flagged'] == flag_info["good flag"]) / len(df[col + '_flagged'])

        # if wind direction column from lidar, need to store height
        if 'm Wind Direction (°)' in col:
            if not '_flagged' in col:
                # Measurement Height
                stats_table[col]['Height'] = np.double(col.replace('m Wind Direction (°)',''))


        # if wind speed column from lidar, need to calculate some other stats and some differently
        if 'm Wind Speed (m/s)' in col:
            if not '_flagged' in col:
                # Measurement Height
                stats_table[col]['Height'] = np.double(col.replace('m Wind Speed (m/s)',''))            

                # Gust - grabs max data from maximum column
                stats_table[col]['Gust']  = df[col.replace('(m/s)','max (m/s)')].max()

                ws_data = np.array(df[col])
                sd_data = np.array(df[col.replace('(m/s)', 'Dispersion (m/s)')])

                # Average Std
                stats_table[col]['Std'] = np.nanmean(sd_data)

                # Average TI
                stats_table[col]['Avg TI']  = 100 * np.nanmean(sd_data / ws_data)
            
                # IEC (15 m/s)
                index = np.arange(0,np.shape(ws_data)[0],1)
                index_WS15 = index[np.where((ws_data < 15.5) & (ws_data >= 14.5))]
                stats_table[col]['IEC 15'] = 100 * float(np.mean (sd_data[index_WS15] / ws_data[index_WS15])) 

                # Wind power density
                index_not_nan = index[np.logical_not(np.isnan(ws_data))]
                ws_data_not_nan = ws_data[index_not_nan]

                WPDStore = np.ndarray((np.shape(ws_data_not_nan)[0],1))
                for c001 in range(0,np.shape(ws_data_not_nan)[0]):
                    WPDStore[c001] = 0.5 * float(air_density) * math.pow(ws_data_not_nan[c001],3)
                stats_table[col]['WPD'] = np.double(np.nanmean(WPDStore))
        
        # if solar radiation ....
        if 'SR' in col:
            if not '_flagged' in col:
                # Turn Watts into kW
                stats_table[col]['Tot'] = stats_table[col]['Tot'] / 1000


    return stats_table









def plot_wind_correlation_by_direction(folder,df,hub_height):
    '''

    '''
    # SETUP
    max_speed = 30
    speed_step = 2



    # PLOT CORRELATIONS THROUGH EACH SECTOR

    # Declare 8 wind direction sectors, with 337.5 -> 22.5 last end sector
    direction_bins = np.arange(22.5,382.5,45)
    # Use hub height wind direction for analysis
    direction_col = [col for col in df.columns if str(hub_height[1]) + 'm Wind Direction (' in col and not '_flagged' in col]
    print(direction_col)
    direction_data = np.array(df[direction_col])

    plt.figure(figsize=[25,25])
    plot_order = [3,6,9,8,7,4,1,2]
    sector_names = ['NE', 'E', 'SE', 'S', 'SW', 'W', 'NW', 'N']
    c001 = 0
    for b in direction_bins:

        # SELECT SUBPLOT
        ax=plt.subplot(3, 3, plot_order[c001])
        
        # Find index where data in each direction sector
        if c001 < 7:
            l = b
            u = b + 45
            direction_index = np.where((direction_data >= l) & (direction_data < u))
            freq = 100 * len(direction_index[0]) / len(direction_data)
        if c001 == 7: # Northern section from 337.5 -> 22.5
            l = b
            u = 22.5           
            direction_index = np.where((direction_data >= l) | (direction_data < u)) 
            freq = 100 * len(direction_index[0]) / len(direction_data)


        # Extract wind speed data
        wspeed_data_bot = np.array(df[str(hub_height[0]) + 'm Wind Speed (m/s)'])
        wspeed_data_hub = np.array(df[str(hub_height[1]) + 'm Wind Speed (m/s)'])
        wspeed_data_top = np.array(df[str(hub_height[2]) + 'm Wind Speed (m/s)'])

        wspeed_data_bot_dir = wspeed_data_bot[direction_index[0]]
        wspeed_data_hub_dir = wspeed_data_hub[direction_index[0]]
        wspeed_data_top_dir = wspeed_data_top[direction_index[0]]


        # HUB TO TOP
        # Calculate Regression line after removing nan data
        not_nan_index = np.where((np.logical_not(np.isnan(wspeed_data_top_dir))
            & (np.logical_not(np.isnan(wspeed_data_hub_dir)))))
        wspeed_data_hub_dir_c = wspeed_data_hub_dir[not_nan_index]              
        wspeed_data_top_dir_c = wspeed_data_top_dir[not_nan_index]

        # Calculate line
        A = np.vstack([wspeed_data_hub_dir_c, np.ones(len(wspeed_data_hub_dir_c))]).T
        m,c=np.linalg.lstsq(A,wspeed_data_top_dir_c)[0]
        regress_line_hub_top=m*np.arange(0,40,1)+c        

        # Plot
        plt.plot(wspeed_data_hub_dir,wspeed_data_top_dir, 
            color = 'lightsteelblue', marker = '.', linestyle = '', alpha = 0.5)

        # Plot regression line
        ax.plot(np.arange(0,40,1),regress_line_hub_top,'navy', linewidth = 2)


        # HUB TO BOTTOM
        # Calculate Regression line after removing nan data
        not_nan_index = np.where((np.logical_not(np.isnan(wspeed_data_hub_dir))
            & (np.logical_not(np.isnan(wspeed_data_bot_dir)))))
        wspeed_data_hub_dir_c = wspeed_data_hub_dir[not_nan_index]              
        wspeed_data_bot_dir_c = wspeed_data_bot_dir[not_nan_index]

        # Calculate line
        A = np.vstack([wspeed_data_hub_dir_c, np.ones(len(wspeed_data_hub_dir_c))]).T
        m,c=np.linalg.lstsq(A,wspeed_data_bot_dir_c)[0]
        regress_line_hub_bot=m*np.arange(0,40,1)+c        

        # Plot
        plt.plot(wspeed_data_hub_dir,wspeed_data_bot_dir, 
            color = 'palegreen', marker = '.', linestyle = '', alpha = 0.5)

        # Plot regression line
        ax.plot(np.arange(0,40,1),regress_line_hub_bot,'forestgreen', linewidth = 2)


        # FIGURE ATTRIBUTES
        # Plot 1:1 line to see correlation
        plt.plot([-1,40],[-1,40],'gold', linewidth = 2)

        # Plot title and axis labels
        plt.title(sector_names[c001] + ' (' + "%3.1f" %freq + '%)', fontsize = 30)
        plt.xlabel('Hub height', fontsize = 20)   
        plt.ylabel('Blade top/bottom height', fontsize = 20)        

    
        plt.xlim(0, max_speed)
        xTicks = np.arange(0,max_speed,speed_step)
        plt.xticks(xTicks, fontsize = 15)

        plt.ylim(0, max_speed)
        yTicks = np.arange(0,max_speed,speed_step)
        plt.yticks(yTicks, fontsize = 15)

        plt.grid(linestyle='dotted', color='grey') 


        c001 += 1


    # ALL DIRECTIONS PLOT IN CENTRE
    ax=plt.subplot(3, 3, 5)

    # Extract wind speed data
    wspeed_data_bot = np.array(df[str(hub_height[0]) + 'm Wind Speed (m/s)'])
    wspeed_data_hub = np.array(df[str(hub_height[1]) + 'm Wind Speed (m/s)'])
    wspeed_data_top = np.array(df[str(hub_height[2]) + 'm Wind Speed (m/s)'])


    # HUB TO TOP
    # Calculate Regression line after removing nan data
    not_nan_index = np.where((np.logical_not(np.isnan(wspeed_data_top))
        & (np.logical_not(np.isnan(wspeed_data_hub)))))
    wspeed_data_hub_c = wspeed_data_hub[not_nan_index]              
    wspeed_data_top_c = wspeed_data_top[not_nan_index]

    # Calculate line
    A = np.vstack([wspeed_data_hub_c, np.ones(len(wspeed_data_hub_c))]).T
    m,c=np.linalg.lstsq(A,wspeed_data_top_c)[0]
    regress_line_hub_top=m*np.arange(0,40,1)+c        

    # PLOT DATA HUB TO TOP
    plt.plot(wspeed_data_hub,wspeed_data_top, 
        color = 'lightsteelblue', marker = '.', linestyle = '', alpha = 0.5)

    # Plot regression line
    ax.plot(np.arange(0,40,1),regress_line_hub_top,'navy', linewidth = 2)


    # HUB TO BOTTOM
    # Calculate Regression line after removing nan data
    not_nan_index = np.where((np.logical_not(np.isnan(wspeed_data_hub))
        & (np.logical_not(np.isnan(wspeed_data_bot)))))
    wspeed_data_hub_c = wspeed_data_hub[not_nan_index]              
    wspeed_data_bot_c = wspeed_data_bot[not_nan_index]

    # Calculate line
    A = np.vstack([wspeed_data_hub_c, np.ones(len(wspeed_data_hub_c))]).T
    m,c=np.linalg.lstsq(A,wspeed_data_bot_c)[0]
    regress_line_hub_bot=m*np.arange(0,40,1)+c        

    # PLOT DATA HUB TO TOP
    plt.plot(wspeed_data_hub,wspeed_data_bot, 
        color = 'palegreen', marker = '.', linestyle = '', alpha = 0.5)

    # Plot regression line
    ax.plot(np.arange(0,40,1),regress_line_hub_bot,'forestgreen', linewidth = 2)




    # FIGURE ATTRIBUTES
    # Plot 1:1 line to see correlation
    plt.plot([-1,40],[-1,40],'gold', linewidth = 2)

    # Plot title and axis labels
    plt.title('All Sectors', fontsize = 30)
    plt.xlabel('Hub height', fontsize = 20)
    plt.ylabel('Blade top/bottom height', fontsize = 20)    
       
    
    # SET GRAPH ATTRIBUTES
    plt.xlim(0, max_speed)
    xTicks = np.arange(0,max_speed,speed_step)
    plt.xticks(xTicks, fontsize = 15)

    plt.ylim(0, max_speed)
    yTicks = np.arange(0,max_speed,speed_step)
    plt.yticks(yTicks, fontsize = 15)
    plt.grid(linestyle='dotted', color='grey') 


    # Save figure
    plt.savefig(folder + '/FIG_direction_correlation', bbox_inches='tight')
    plt.close()




def plot_wind_shear(folder,df,hub_height,alts):
    '''

    '''

    # 20190122 - change 11 -> len(alts) allowed dynamic arrays to be created depending on number of alts specified
    # SETUP
    max_speed = 45
    speed_step = 1

    # Declare 8 sectors, with 337.5 -> 22.5 last end sector
    direction_bins = np.arange(22.5,382.5,45)
    sector_names = ['NE', 'E', 'SE', 'S', 'SW', 'W', 'NW', 'N']
    # Use hub height wind direction for analysis
    direction_col = [col for col in df.columns if str(hub_height[1]) + 'm Wind Direction (' in col and not '_flagged' in col]
    direction_data = np.array(df[direction_col])

    # Extract speed column names
    speed_cols = [col for col in df.columns if 'm Wind Speed (' in col and not '_flagged' in col]

    
    # Declare arrays in which to hold shear data
    shear_matrix_by_sector = np.ndarray((8,len(alts),3)) # 20190122
    shear_matrix_all_sectors = np.ndarray((len(alts),3)) # 20190122
    shear_sectors = np.ndarray((8,2))

    # Variables for alpha calculation
    maxWindShearHgt = 200
    windShear_vertProfile=np.arange(0,maxWindShearHgt,0.1)
    alpha_calc_var = np.ndarray((9,2))


    # CALCULATE WIND SHEAR DATA AND STORE IN ARRAYS
    # Go through each declared wind direction sector
    c001 = 0
    for b in direction_bins:
      
        
        # Find index where data in each sector
        if c001 < 7:
            l = b
            u = b + 45
            shear_sectors[c001,0] = l
            shear_sectors[c001,1] = u
            direction_index = np.where((direction_data >= l) & (direction_data < u))
        else:
            l = b
            u = 22.5    
            shear_sectors[c001,0] = l
            shear_sectors[c001,1] = u        
            direction_index = np.where((direction_data >= b) | (direction_data < u))

        # Go through each height and calculate wind speed data
        c002 = 0
        for c in speed_cols:
            # Extract wind speed data for each height
            speed_data_avg = np.array(df[c])
            speed_data_max = np.array(df[c.replace('(m/s)','max (m/s)')])
            speed_data_min = np.array(df[c.replace('(m/s)','min (m/s)')])

            # Calulate avg, max, min for each height for all sectors (this will be over written each time 'b' loops)
            shear_matrix_all_sectors[c002,0] = np.nanmean(speed_data_avg)
            shear_matrix_all_sectors[c002,1] = np.nanmax(speed_data_max)
            shear_matrix_all_sectors[c002,2] = np.nanmin(speed_data_min)

            # Calulate avg, max, min for each height by sector
            shear_matrix_by_sector[c001,c002,0] = np.nanmean(speed_data_avg[direction_index[0]])
            shear_matrix_by_sector[c001,c002,1] = np.nanmax(speed_data_max[direction_index[0]])
            shear_matrix_by_sector[c001,c002,2] = np.nanmin(speed_data_min[direction_index[0]])


            # Calculate alpha value
            speed_lower = df[str(hub_height[0]) + 'm Wind Speed (m/s)']
            speed_lower_avg_sect = np.mean(speed_lower[direction_index[0]])
            speed_lower_avg_all = np.mean(speed_lower)

            speed_upper = df[str(hub_height[2]) + 'm Wind Speed (m/s)']
            speed_upper_avg_sect = np.mean(speed_upper[direction_index[0]])
            speed_upper_avg_all = np.mean(speed_upper)

            wind_speed_hub = df[str(hub_height[1]) + 'm Wind Speed (m/s)']
            alpha_calc_var[c001,1] = np.mean(wind_speed_hub[direction_index[0]])
            alpha_calc_var[8,1] = np.mean(wind_speed_hub)

            alpha_calc_var[c001,0] = np.log10(speed_upper_avg_sect / speed_lower_avg_sect) / np.log10(float(hub_height[2]) / float(hub_height[0])) # by direction
            alpha_calc_var[8,0] = np.log10(speed_upper_avg_all / speed_lower_avg_all) / np.log10(float(hub_height[2]) / float(hub_height[0])) # all directions
            
            c002 += 1
        c001 += 1


    # PLOT
    plot_order = [3,6,9,8,7,4,1,2,5]
    plt.figure(figsize=[25,25])
    for x in range(0,9):


        wind_shear_mean=(pow((windShear_vertProfile / hub_height[1]), alpha_calc_var[x,0])) * alpha_calc_var[x,1]

        # SELECT SUBPLOT
        ax=plt.subplot(3, 3, plot_order[x])

        # Plot by sector
        if x < 8:
            plt.plot(wind_shear_mean,windShear_vertProfile, color='k', linewidth = 6, alpha = 0.5)
            plt.plot(shear_matrix_by_sector[x,:,0],alts, color = 'limegreen', marker = 'o', linewidth = 2)
            plt.plot(shear_matrix_by_sector[x,:,1],alts, color = 'royalblue', marker = 'o', linewidth = 2)
            plt.plot(shear_matrix_by_sector[x,:,2],alts, color = 'crimson', marker = 'o', linewidth = 2)
            plt.title(sector_names[x] + ': ' + (r'$\alpha$') + '= ' + "%.2f" % alpha_calc_var[x,0], fontsize=30)

        # Plot all sector graph in centre
        else:
            plt.plot(wind_shear_mean,windShear_vertProfile, color='k', linewidth = 6, alpha = 0.5)
            plt.plot(shear_matrix_all_sectors[:,0],alts, color = 'limegreen', marker = 'o', linewidth = 2)
            plt.plot(shear_matrix_all_sectors[:,1],alts, color = 'royalblue', marker = 'o', linewidth = 2)
            plt.plot(shear_matrix_all_sectors[:,2],alts, color = 'crimson', marker = 'o', linewidth = 2)
            plt.title('All Sectors: ' + (r'$\alpha$') + '= ' + "%.2f" % alpha_calc_var[x,0], fontsize=30)

        # Plot shaded turbine hub height and blade band
        plt.fill_between(np.arange(0,max_speed + speed_step),hub_height[0],hub_height[2], alpha = 0.1, color = 'powderblue')
        plt.plot(np.arange(0,max_speed + speed_step), (0 * np.arange(0,max_speed + speed_step)) + hub_height[1], color = 'powderblue')


        plt.xlim(0, max_speed)
        xTicks = np.arange(0,45,5)
        plt.xticks(xTicks, fontsize = 20)
        plt.xlabel('Wind Speed (ms$^{-1}$)', fontsize=20)

        plt.ylim(0,alts[len(alts)-1])
        plt.yticks(alts, fontsize = 20)
        plt.ylabel('Height (m)', fontsize=20)
        plt.grid(linestyle='dotted', color='grey') 
    
    # Save figure
    plt.savefig(folder + '/FIG_wind_shear', bbox_inches='tight')
    plt.close()




def plot_data_availability(folder,df,alts,Ylabel,fins):
    '''

    '''
    # Arrange figure
    figSize=(40,5)
    fig=plt.figure(figsize=figSize)

    # Variables
    bin_levels=np.arange(fins[0],fins[1],fins[2])
    array=df.values

    # Filled contour plot
    plt.pcolor(array.T,cmap= 'bone', vmin = 0, vmax = 100)
    cbar = plt.colorbar()
    cbar.set_label(Ylabel)

    # X tick labels - daily
    xticks = [dt.to_pydatetime().date() for dt in df.index if dt.hour == 0 and dt.minute == 0]
    index_ideal = np.arange(0,len(df),1)
    index_min = index_ideal[np.where(df.index.minute == 0)]
    index_hrs = index_ideal[np.where(df.index.hour == 0)]
    index=np.sort(list(set(index_min) & set(index_hrs)))
    plt.xticks(index,xticks,rotation=90)
    plt.xlim(0, len(df))

    # Y axis ticks and label
    plt.yticks(np.arange(0,len(alts)),alts)
    plt.ylabel('Altitude (m)')
    plt.ylim(0,len(df.columns))
    plt.grid(linestyle='dotted', color='grey') 

    # Save figure
    plt.savefig(folder + '/FIG_data_availability', bbox_inches='tight')
    plt.close()



def plot_data_validation(folder, df_lidar, df_tower, filename, max_Value, step_Value, xtick_space, y_lim_max):
    '''

    '''
    # MERGE LIDAR AND TOWER DATA
    df_tower = df_tower.add_prefix('T ')
    df_lidar = df_lidar.add_prefix('L ')
    df_all = pd.merge(left=df_lidar, right=df_tower, left_index=True, right_index=True, how='left')



    
    # REMOVE ALL DATA EXCEPT THOSE WITH COMMON HEIGHTS
    # Extract heights from column names
    heights_all = []
    
    for c009 in range(0, len(df_all.columns)):
        name = df_all.columns[c009]

        temp = re.findall('\d+', name)
        heights_all.append(int(temp[0]))
 
    # Find duplicate heights
    heights_duplicate = [item for item, count in collections.Counter(heights_all).items() if count > 1]
    index = []
    for a in heights_duplicate:
        c001 = 0
        for b in heights_all:
            if b == a:
                index.append(c001)
            c001 += 1

    # Reduce columns in dataframe to those with common heights
    df_all = df_all[df_all.columns[index]]


    # RENAME COLUMNS, KEEPING ONLY SOURCE NAME, HEIGHT AND ORIENTATION, AND DEFINE HISTOGRAM COLOURS
    hist_Fcolors = []
    hist_Ecolors = []
    for col in df_all.columns:
        temp = re.findall('\d+', col)
        if 'T' in col:
            hist_Fcolors.append('forestgreen')
            hist_Ecolors.append('palegreen')
            df_all.rename(columns ={col: 'TOWER: ' + temp[0] + col[col.find(temp[0])+len(temp[0]):len(col)]}, inplace =True)
        else:
            hist_Fcolors.append('dodgerblue')
            hist_Ecolors.append('paleturquoise')            
            df_all.rename(columns ={col: 'LIDAR: ' + temp[0]}, inplace =True)


    # START PLOTTING, HISTOGRAMS ON DIAGONAL, SCATTER ON UPPER, AND COEFF on LOWER
    max_Value = max_Value + step_Value
    xTicks = np.arange(0,max_Value,xtick_space)
    A=[-1,max_Value] # regression line?
    num_sensors = len(df_all.columns)

    plt.figure(figsize=[40,40])
    c001 = 0  # data_A counter
    c003 = 1  # subplot counter
    for name_A in df_all.columns:
        c002=0 # data_B counter
        for name_B in df_all.columns:

            # SELECT SUBPLOT
            ax=plt.subplot(num_sensors, num_sensors, c003)

            # PLOT SINGLE HISTOGRAMS ON DIAGONAL
            if c001 == c002:
                data_temp = np.array(df_all[name_A])

                # If there is data
                if len(data_temp)>0:
                    levels = np.arange(0, max_Value, step_Value)
                    dataHist=np.zeros(len(levels))
                    dataXloc=np.zeros(len(levels))

                    c007=0
                    for c006 in range(0,len(levels)):
                        lower = levels[c006]
                        upper = levels[c006] + step_Value
                        index_dataHist = np.where((data_temp > lower) & (data_temp <= upper))
                        dataXloc[c007] = lower
                        dataHist[c007] = np.shape(index_dataHist)[1]/float(np.shape(data_temp)[0])
                        c007 += 1
                else:
                    dataXloc = 0
                    dataHist = 0

                # Plot
                plt.bar(dataXloc, dataHist, width = step_Value, facecolor=hist_Fcolors[c001], edgecolor=hist_Ecolors[c001])

                # Set axis limits
                plt.title(name_B, fontsize = 25)
                plt.xlim(0,xTicks[len(xTicks)-1])
                plt.ylim(0,y_lim_max)
                plt.xticks(xTicks, fontsize = 20)
                plt.yticks(fontsize = 20)
                ax.grid(linestyle='dotted', color='grey') 

            # PLOT SCATTERPLOTS
            if c001 < c002:

                # Get data
                data_A = np.array(df_all[name_A])
                data_B = np.array(df_all[name_B])

                # if there is data for both
                if (len(data_A) > 0 and len(data_B) > 0) :
                    ax.plot(data_B,data_A,'r.',alpha=0.1, color = 'dimgray')

                    ## Calculate Regression line after removing nan data
                    try: 
                        not_nan_index = np.where((np.logical_not(np.isnan(data_A)) & (np.logical_not(np.isnan(data_B)))))
                        data_A_c = data_A[not_nan_index]              
                        data_B_c = data_B[not_nan_index]

                        # Calculate line
                        A = np.vstack([data_B_c, np.ones(len(data_A_c))]).T
                        m,c=np.linalg.lstsq(A,data_A_c)[0]
                        regress_line=m*np.arange(0,362,1)+c        

                        # Plot regression line
                        ax.plot(np.arange(0,362,1),regress_line,'slategrey', linewidth = 2)

                    except:
                    	a=1

                # plot 1:1 line to see correlation
                ax.plot([-1,xTicks[len(xTicks)-1]+40],[-1,xTicks[len(xTicks)-1]+40],'gold', linewidth = 2)
                
                # set axis limits
                plt.xlim(0,xTicks[len(xTicks)-1])
                plt.ylim(0,xTicks[len(xTicks)-1])
                plt.xticks(xTicks, fontsize = 20)
                plt.yticks(xTicks, fontsize = 20)
                ax.grid(linestyle='dotted', color='grey') 

            # PLOT CORRELATION COEFFS 
            if c001 > c002:

                data_Coeff = df_all[name_A].corr(df_all[name_B]) 
                ax.text(0.5, 0.5, round(data_Coeff,2), verticalalignment = 'center', 
                        horizontalalignment = 'center',color = 'black', fontsize = 40)

                # hide tick marks
                ax.axes.get_xaxis().set_visible(False)

                ax.set_xticks(np.linspace(2,4,1))
                ax.set_yticks(np.linspace(2,4,1))
        
            # next plot
            c002 += 1
            c003 += 1

        c001 += 1

        
    
    #mpl.rc('font', **fontSpec)
    # Save figure
    plt.savefig(folder + '/FIG_validation_' + filename, bbox_inches='tight')
    plt.close()




def plot_timeseries_contour(folder,name,df,alts,Ylabel,fins):
    '''


    '''
    # Arrange figure
    figSize=(40,12)
    fig=plt.figure(figsize=figSize)

    # Variables
    bin_levels=np.arange(fins[0],fins[1],fins[2])
    array=df.values

    # Filled contour plot
    plt.contourf(array.T,cmap= 'jet',levels=bin_levels)
    cbar = plt.colorbar()
    cbar.set_label(Ylabel)

    # X tick labels - daily
    xticks = [dt.to_pydatetime().date() for dt in df.index if dt.hour == 0 and dt.minute == 0]
    index_ideal = np.arange(0,len(df),1)
    index_min = index_ideal[np.where(df.index.minute == 0)]
    index_hrs = index_ideal[np.where(df.index.hour == 0)]
    index=np.sort(list(set(index_min) & set(index_hrs)))
    plt.xticks(index,xticks,rotation=90)
    plt.xlim(0, len(df))

    # Y axis ticks and label
    plt.yticks(np.arange(0,len(alts)),alts)
    plt.ylabel('Altitude (m)')
    plt.grid(linestyle='dotted', color='grey') 

    # Save figure
    plt.savefig(folder + '/FIG_timeseries_contour_' + name, bbox_inches='tight')
    plt.close()




def plot_timeseries_line(folder,df,name,yTickValues,yLabel,flag_info,**kwargs):
    '''


    '''

    # KWARGS
    figSize      = kwargs.pop('figSize', (40,12))
    #bdrWths      = kwargs.pop('bdrwths',[1.05,0,0.05,0.95])
    bdrWths      = kwargs.pop('bdrwths',[1.05,0,0.1,0.95])
    chartType    = kwargs.pop('chartType', 'line')
    lineStyle    = kwargs.pop('lineStyle','-')
    lineWidth    = kwargs.pop('lineWidth',3)
    markerType   = kwargs.pop('markerType','.')
    markerSize   = kwargs.pop('markerSize',1)
    barWidth     = kwargs.pop('barWidth',0)
    plotLegend   = kwargs.pop('plotLegend',1) # normally plot legend
    plotFlags    = kwargs.pop('plotFlags',1) # normally plot flags
    goodFlag     = kwargs.pop('goodFlag',8)
    yTickValues2 = kwargs.pop('yTickValues2',[0,10,20,30,40,50,60,70])

    # FIGURE ATTRIBUTES
    fig1=plt.figure(figsize=figSize)
    gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.15,right=0.95)
    ax0 = fig1.add_subplot(gs[0 :70,0:95])

    # Add padding around plot
    plt.subplots_adjust(left=0.1, right = 1., top=1., bottom = 0.1) #
    fig1.text(0.5,  bdrWths[0],".", fontsize=1, ha='center', va='center')   #top
    fig1.text(0.5,  bdrWths[1], ".", fontsize=1, ha='center', va='center')  #bottom
    fig1.text(bdrWths[2], 0.5, ".", fontsize=1, ha='center', va='center')   #left
    fig1.text(bdrWths[3], 0.5, ".", fontsize=1, ha='center', va='center')   #right


    # PLOT DATA
    # Seperate data and flag column names
    data_cols = []
    flag_cols = []
    for col in df.columns:
        if 'flagged' in col:
            flag_cols.append(col)
        else:
            data_cols.append(col)

    # Remove text from sensor label for legend
    stringReplace = ''.join([i for i in data_cols[0] if not i.isdigit()])

    # line colours
    colors = [
    'deepskyblue',
    'darkorange',
    'lime',
    'olive',
    'crimson',
    'indigo',
    'violet',
    'mediumblue',
    'forestgreen',
    'gold',
    'teal'
    ]


    # Check if line or bar type graph
    if chartType=='line':
        c001 = 0
        for sensor in data_cols: 
            ax0.plot(df.index, df[sensor], label=sensor.replace(stringReplace,""),color = colors[c001],linestyle=lineStyle,linewidth=lineWidth,marker=markerType,markersize=markerSize)
            c001 += 1

    if chartType=='bar':
        for sensor in data_cols: 
            ax0.bar(df.index, df[sensor],  label=sensor.replace(stringReplace, ""), width=barWidth, facecolor = 'darkblue', edgecolor='darkblue' )


    # Set plot attributes
    ax0.set_ylabel(yLabel,fontsize = 50)
    plt.setp(ax0.get_yticklabels(), fontsize=40)
    ax0.set_ylim(yTickValues[0],yTickValues[len(yTickValues)-1])
    plt.setp(ax0,yticks=yTickValues)

    ax0.set_xlim(min(df.index), max(df.index))
    # X tick labels - daily
    xticks = [dt.to_pydatetime().date() for dt in df.index if dt.hour == 0 and dt.minute == 0]
    index_ideal = np.arange(0,len(df),1)
    index_min = index_ideal[np.where(df.index.minute == 0)]
    index_hrs = index_ideal[np.where(df.index.hour == 0)]
    index=np.sort(list(set(index_min) & set(index_hrs)))



    # Add legend
    if plotLegend == 1 and len(data_cols) > 1:
        ax0.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=len(data_cols), mode="expand", borderaxespad=0., fontsize = 30)
        




    # Add black border around plot
    ax0.spines['bottom'].set_color('black')
    ax0.spines['right'].set_color('black')
    ax0.spines['left'].set_color('black')
    ax0.tick_params(top=False, right=False, bottom=False)
    ax0.grid(zorder=2, color='grey', linestyle='dotted')

    
    # Add cumulative line if bar chart
    if chartType=='bar':
        ax1 = ax0.twinx()
        for sensor in data_cols: ax1.plot(df.index, np.cumsum(df[sensor]), label=sensor.replace(stringReplace, ""), linestyle=lineStyle,linewidth=lineWidth,color='deepskyblue')   #DIFF
        
        ax1.set_ylabel('Total (mm)',fontsize = 50)
        plt.setp(ax1.get_yticklabels(), fontsize=40)
        ax1.set_ylim(yTickValues2[0],yTickValues2[len(yTickValues2)-1])
        plt.setp(ax1, yticks=yTickValues2)
        
        ax1.set_xlim(min(df.index), max(df.index))
        ax1.tick_params(top=False,right=False,bottom=False)
        



    # PLOT FLAGS
    if len(flag_cols) > 0:

        flag_colours = flag_info['flag colours']

        # Quash x ticks from being displayed on top plot
        plt.setp(ax0.get_xticklabels(), visible=False)

        # Add new subplot for flags
        ax2 = fig1.add_subplot(gs[70:70+len(flag_cols)*6  ,0:95])
        # SET TO AVOID THE ERROR BELOW ON THE LINE BELOW WITH THE ******'s
        #  a value is trying to be set on a copy of a slice from a dataframe
        pd.options.mode.chained_assignment = None  # default='warn'

        # Display flags under time series plot
        ax2.set_ylim(0,len(flag_cols))
        ax2.set_xlim(min(df.index), max(df.index))
        sequence=-1*np.sort(-1*np.arange(0,len(flag_cols),1)) # flips way in which sensors are accessed

        for cnt01 in range(len(flag_cols)):
            sensor_i = sequence[cnt01]
            flag_arr = df[flag_cols[sensor_i]]
            flag_arr_shift = np.insert(np.copy(flag_arr)[:-1], 0, flag_arr[0])
           
            for loc in np.where(flag_arr_shift == goodFlag): flag_arr_shift[loc] = flag_arr[loc]

            # find location of first good flag
            for loc in np.where(flag_arr == goodFlag)[0]:
                # ?????
                if not loc == flag_arr.shape[0]-1: flag_arr[loc+1] = goodFlag

            for i in range(1,11):
                # plot
                ax2.fill_between(df.index, (cnt01 + 1), cnt01, where= flag_arr_shift == i, color = flag_colours[str(i)])
                # plot good flag bar
                ax2.fill_between(df.index, (cnt01 + 1), cnt01, where= flag_arr == goodFlag, color='w')
                # add black horizontal line between flag lines
                ax2.plot(df.index,(cnt01+1)*np.ones(len(df.index)),'gray',linewidth=2)
    

        ticks = [x for x in df.index if x.hour == 0 and x.minute == 0]
        ax2.spines['top'].set_color('gray')
        plt.setp(ax2.get_xticklabels(), rotation=30, fontsize=40)
        #plt.setp(index,xticks, rotation=30, fontsize=40)
        ax2.tick_params(top=False, right=False)
        ax2.set_yticks([])
        ax2.xaxis.grid(zorder=2, color='grey', linestyle='dotted')
    else:
        #ax0.xticks(index,xticks,rotation=90)
        plt.setp(ax0.get_xticklabels(), rotation=30, fontsize=40)


    
    '''Save figure'''
    plt.savefig(folder + '/FIG_timeseries_' + name ,bbox_inches='tight')
    plt.close()



def plot_wind_rose(folder,df_WS,df_WD,binsSpec,name,textLabel):
    '''
    max radii of plot needs to be set manually in windrose.py -> def _update(self, **kwargs):

    '''

    fontSpec= {'size': 20}
    mpl.rc('font', **fontSpec)

    #legendTitleLoc = kwargs.pop('legendTitleLoc',(0.75,0.25))

    from windrose import WindroseAxes

    def new_axes():
        fig = plt.figure(figsize=figSize, dpi=80, facecolor='w', edgecolor='w')
        rect = [0.1, 0.1, 0.8, 0.8]
        ax = WindroseAxes(fig, rect) # BOB , axisbg='w')
        # ORIGINAL ax = WindroseAxes(fig, rect, axisbg='w')
        fig.add_axes(ax)
        return ax

    figSize = (8,8)
    ax = new_axes()

    
    ax.bar(df_WD.values, df_WS.values, normed=True, opening=0.8, edgecolor='white', bins=np.linspace(binsSpec[0],binsSpec[1],binsSpec[2]))

    kwargs = {'prop':'larger'}

    def set_legend(ax, **kwargs):
        #loc is location of legend
        l = ax.legend(borderaxespad=-0.10,loc = (1.1, -0.1))
        # centered legend with 2 columns for legendBox for annual windroses
        plt.setp(l.get_texts(), fontsize=20)

    set_legend(ax)
    plt.annotate(textLabel,xy=(65,6),xytext=(0.82,0.30),textcoords='figure fraction', fontsize=22)
    # original (0.75,0.4)

    plt.draw()

    plt.savefig(folder + '/FIG_WR_' + name, bbox_inches='tight')
    plt.close()

    mpl.rcdefaults()


def plot_WS_daily(folder,name,df,alts):
    '''

    '''
    # Arrange figure
    figSize=(15,5)
    fig=plt.figure(figsize=figSize)

    # Variables
    windLevels=np.arange(0,18,0.5)
    array=df.values

    # Filled contour plot
    plt.contourf(array.T,cmap= 'jet',levels=windLevels)
    cbar = plt.colorbar()
    cbar.set_label('Wind Speed (ms$^{-1}$)')

    # Contour lines with contour labels
    CS = plt.contour(array.T,colors='black',levels=windLevels)  
    plt.clabel(CS, inline=1, fontsize=10,fmt = '%0.1f')  

    # X axis attributes
    plt.xticks(np.arange(0,24))
    plt.xlabel('Hour of the day')

    # Y axis ticks and label
    plt.yticks(np.arange(0,len(alts)),alts)
    plt.ylabel('Height (m)')

    plt.grid(linestyle='dotted', color='grey') 




    # Save figure
    plt.savefig(folder + '/FIG_daily_WS' + name,bbox_inches='tight')
    plt.close()




def plot_WS_monthly(folder,name,df,alts):
    '''

    '''

    month_names = ['n/a', 'J', 'F', 'M', 'A', 'M ', 'J', 'J', 'A', 'S', 'O', 'N', 'D']
    # Extract months and find unique in order
    df['month'] = df.index.month
    indexes = np.unique(df['month'].values, return_index=True)[1]
    months_in_data = [df['month'].values[index] for index in sorted(indexes)]
    monthly_WS_average = np.ndarray((len(alts),len(months_in_data)))

    # month names for y axis of graph
    month_names_in_data = []


    c001 = 0
    for m in months_in_data:
        month_names_in_data.append(month_names[m])

        c002 = 0
        for alt in alts:

            temp_WS_data = df[str(alt)+ 'm Wind Speed (m/s)']
            mean_monthly_value_at_alt = temp_WS_data[temp_WS_data.index.month == m].mean()
            monthly_WS_average[c002,c001] = mean_monthly_value_at_alt

            c002 += 1
        c001 += 1 


    # Arrange figure
    figSize=(20,6)
    fig=plt.figure(figsize=figSize)

    # Variables
    windLevels=np.arange(0,18,0.5)

    # Filled contour plot
    plt.contourf(monthly_WS_average, cmap= 'jet', levels=windLevels)
    cbar = plt.colorbar()
    cbar.set_label('Wind Speed (ms$^{-1}$)', fontsize=30)
    cbar.ax.tick_params(labelsize=20) 

    # Contour lines with contour labels
    CS = plt.contour(monthly_WS_average,colors='black',levels=windLevels)  
    plt.clabel(CS, inline=1, fontsize=15,fmt = '%0.1f')  

    # X axis attributes
    plt.xticks(np.arange(0,len(months_in_data)),month_names_in_data, fontsize=20)
    plt.xlabel('Monthly Average', fontsize=20)

    # Y axis ticks and label
    plt.yticks(np.arange(0,len(alts)),alts, fontsize=20)
    plt.ylabel('Height (m)', fontsize=20)

    plt.grid(linestyle='dotted', color='grey') 

    # Save figure
    plt.savefig(folder + '/FIG_monthly_WS' + name,bbox_inches='tight')
    plt.close()



def plot_WD_daily(folder,name,direction_data, Freq_Levels, contour_levels):
    '''
    
    '''

    # Specify direction bins
    WDbins=[0,15,30,45,60,75,90,105,120,135,150,165,180,195,210,225,240,255,270,285,300,315,330,345,360]
    WD_freq=np.ndarray((len(WDbins)-1,24))
    
    # Loop through hours in the day
    for c003 in range(0,24,1):
        df = direction_data[direction_data.index.hour == c003]
        data_by_hour = df.values
        
        for c001 in range(0,len(WDbins)-1,1):
            temp = np.sum((data_by_hour >= WDbins[c001]) & (data_by_hour < WDbins[c001+1]))
            WD_freq[c001,c003] = 100 * temp / np.shape(data_by_hour)[0] 
        
    # Arrange figure
    figSize=(15,5)
    fig=plt.figure(figsize=figSize)

    # Filled contour plot
    plt.contourf(WD_freq,contour_levels,cmap= 'jet' )    
    cbar = plt.colorbar()
    cbar.set_label('Frequency (%)')
    
    
    CS = plt.contour(WD_freq,colors='black',levels=Freq_Levels)  
    plt.clabel(CS, inline=1, fontsize=10,fmt = '%0.1f')  


    # X axis attributes
    plt.xticks(np.arange(0,24))
    plt.xlabel('Hour of the day')

    # Y axis ticks and label
    plt.yticks(np.arange(0,24,3),['0','45','90','135','180','225','270','315','360'],)
    plt.ylabel('Wind Direction ($^{o}$)')

    plt.grid(linestyle='dotted', color='grey') 

    # Save figure
    plt.savefig(folder + '/FIG_daily_WD' + name,bbox_inches='tight')
    plt.close()
    



def plot_weibull_distribution(folder,df,fins,name,**kwargs):
    '''

    '''
    figSize    = kwargs.pop('figSize',(15,7))
    weibullON  = kwargs.pop('weibullON',1)


    # plot histogram
    WSHist=np.zeros(int(fins[1]/fins[2]))
    WSXloc=np.zeros(int(fins[1]/fins[2]))

    for c06 in range(0,int(fins[1]/fins[2]),1):
        level=c06*fins[2]
        index_WSHist=np.where((df>level) & (df<=level+fins[2]))
        WSXloc[c06]=level
        WSHist[c06]=np.shape(index_WSHist)[1]/float(np.shape(df)[0])

    plt.figure(figsize=figSize)
    ax=plt.subplot(1,1,1)
    plt.bar(WSXloc, WSHist, width=fins[2], facecolor = 'steelblue', edgecolor = 'cornflowerblue')

    # Construct and plot weibull curve
    if weibullON==1:
        df2 = []
        for x in df.values:
            if not np.isnan(x):
                df2.append(float(x))

        # plot weibull curve
        x = np.linspace(0,fins[1],10000)
        y = exw.fit(df2, floc=0, f0=1)
        y2 = exw.pdf(x, *y)

        # figure details
        plt.plot(x,y2, linewidth=3,color='black')

        # include weibull parameters as legend
        plt.plot(fins[1],0.2, linewidth=1,color='w') # trick to get parameters in plot
        plt.plot(fins[1],0.2, linewidth=1,color='w') # trick to get parameters in plot
        plt.plot(fins[1],0.2, linewidth=1,color='w') # trick to get parameters in plot
        legend0="Weibull Parameters"
        legend1="Mean (m s$^{-1}$): " + "%.2f" % np.mean(df)
        legend2="C (m s$^{-1}$): "+ "%.2f" % y[3]
        legend3= "k: "+ "%.2f" % y[1]
        #fontP = FontProperties()
        #fontP.set_size('small')
        plt.legend((legend0,legend1,legend2,legend3) , loc='upper right' )#,prop = fontP)

    # figure details
    plt.xlim(fins[0],fins[1])
    plt.xticks(fontsize = 15)
    plt.ylim(0,0.3)
    plt.yticks(fontsize = 15)
    
    ax.grid(linestyle='dotted', color='grey') 
    plt.xlabel("Wind Speed (m s$^{-1}$)", fontsize = 20)
    plt.ylabel("Frequency (%)", fontsize = 20)

    # Save figure
    plt.savefig(folder + '/FIG_weibull_distribution_' + name,bbox_inches='tight')
    plt.close()




def plot_turbulence_intensity(folder,data,height):
    '''

    '''
    maxWS = 30
    maxTI = 1.2

    ''' Plot Turbulence Intensity including IEC classes and 15 m/s TI marker '''

    data = data.T

    # Plot layout
    gs = gridspec.GridSpec(2, 1,height_ratios=[4,2])
    fig = plt.figure(figsize=(15,7))
    ax1 = plt.subplot(gs[0])

    # Scatter plot wind speed vs turbulence intensity
    ax1.plot(data[0,:],data[1,:]/data[0,:], color = 'dimgrey', marker = 'o', linestyle='none', linewidth=0, alpha=0.1, zorder=1)

    # Plot IEC class curves
    v = np.arange(0, maxWS, 0.1)
    sigma1_A = 0.16*(0.75*v+5.6)/v
    sigma2_A = 0.14*(0.75*v+5.6)/v
    sigma3_A = 0.12*(0.75*v+5.6)/v

    ax1.plot(v,sigma1_A,'firebrick',linewidth=2)
    ax1.plot(v,sigma2_A,'seagreen',linewidth=2)
    ax1.plot(v,sigma3_A,'royalblue',linewidth=2)
    ax1.legend(('TI', 'IEC Cat A','IEC Cat B', 'IEC Cat C') , loc='upper right', borderpad=0.2, 
        labelspacing=0.05, handletextpad=0.1, fontsize=20)

    # Calculate TI at 15 m/s
    index=np.arange(0,np.shape(data)[1],1)
    index_WS15=index[np.where((data[0,:] < 15+0.5) & (data[0,:] >= 15-0.5))] # between 14.5 and 15.5 m/s
    if len(index_WS15)>0:
        TIat15ms=np.nanmean(data[1,index_WS15]/data[0,index_WS15])
        # plot 15m/s cross - maxFlags+8 is TI_15m/s
        if (not(np.isnan(TIat15ms))):
            ax1.scatter(15,TIat15ms, zorder=3, marker='+',s=600,color='navy',linewidth=1)
            ax1.scatter(15,TIat15ms, zorder=3, marker='s',s=200,color='navy',linewidth=1,facecolors='none')
            plt.text(15.5,-0.05+TIat15ms,'IEC_15',fontsize=20,color='navy',zorder=3)

    # figure details
    plt.ylabel("Turbulence Intensity", fontsize = 25)
    plt.xlim(0,maxWS)
    # hide tick marks
    ax1.get_xaxis().set_visible(False)
    plt.ylim(0,maxTI)
    plt.yticks(fontsize = 20)
    plt.grid(linestyle='dotted', color='grey') 
    #mpl.rc('font', **fontSpec)


    ''' Calculate % of wind speeds relative to IEC curves for wind speed bins'''
    # create new wind speed array and sigma arrays based on increments of 0.5m/s
    maxPercent=15
    increment=0.25


    v2 = np.arange(0, maxWS, increment)
    sigma1_B = 0.16*(0.75*v2+5.6)/v2
    sigma2_B = 0.14*(0.75*v2+5.6)/v2
    sigma3_B = 0.12*(0.75*v2+5.6)/v2
    PercentOver=np.ndarray((3,len(v2)-1))

    index = np.arange(0, np.shape(data)[1], 1)
    for cnt45 in range(0,len(v2)-1,1):
        # for each wind speed in v
        index2=index[np.where((data[0,:]>=v2[cnt45]) & (data[0,:]<v2[cnt45+1]))]
        index3=np.arange(0, len(index2), 1)

        # find % TI in relationship to IEC curves
        index4=index3[np.where(  (data[1,index2]/data[0,index2]) >=sigma3_B[cnt45]  )]
        if len(index4)>0:
            PercentOver[0,cnt45]=100*float(len(index4))/float(len(index2))
        else:
            PercentOver[0,cnt45]=0.

        index4=index3[np.where(  (data[1,index2]/data[0,index2]) >=sigma2_B[cnt45]  )]
        if len(index4)>0:
            PercentOver[1,cnt45]=100*float(len(index4))/float(len(index2))
        else:
            PercentOver[1,cnt45]=0.

        index4=index3[np.where(  (data[1,index2]/data[0,index2]) >=sigma1_B[cnt45]  )]
        if len(index4)>0:
            PercentOver[2,cnt45]=100*float(len(index4))/float(len(index2))
        else:
            PercentOver[2,cnt45]=0.



    # Plot these in bar chart
    ax2 = plt.subplot(gs[1])
    ax2.bar(v2[0:len(v2)-1,], PercentOver[0,:],  increment, color='royalblue',edgecolor='royalblue')
    ax2.bar(v2[0:len(v2)-1,], PercentOver[1,:],  increment, color='seagreen',edgecolor='seagreen')
    ax2.bar(v2[0:len(v2)-1,], PercentOver[2,:],  increment, color='firebrick',edgecolor='firebrick')

    plt.ylabel("Percent (%)", fontsize = 25)
    plt.xlim(0,maxWS)
    plt.xticks(fontsize = 20)
    plt.ylim(0,100)
    plt.yticks([0,25,50,75,100],fontsize = 20)
    plt.xlabel('Wind Speed (m s$^{-1}$)', fontsize = 25)
    plt.grid(linestyle='dotted', color='grey') 
    fig.tight_layout()
    #mpl.rc('font', **fontSpec)

    plt.savefig(folder + '/FIG_turbulance_intensity_' + height ,bbox_inches='tight')
    plt.close()


