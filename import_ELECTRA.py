import os
import time
import pandas as pd
import csv
import codecs

from tkinter import *
from tkinter import filedialog

def import_data(folder,dates):
    #TODO  Check headers the same as last time - if new site create header file

    # TODO DELETE
    print('IMPORTING ELECTRA DATA')    
    data_files_to_import=[]

    # SEARCH FOLDER FOR DATA FILES WITH '.csv' SUFFIX
    original_folder = os.getcwd()
    os.chdir(folder)

    master = Tk()
    master.withdraw() #hiding tkinter window for some reason?
    path = filedialog.askopenfilename(initialdir = folder, title="Select data file", filetypes=[("csv file","*.csv"),("All files",".*")])
    data_files_to_import.append(os.path.basename(path))
    
    # TODO DELETE
    print('IMPORTING: ' + str(data_files_to_import[0]))

    # READ DATA INTO PANDAS ARRAY USING FOUND HEADER LINE NUMBER
    dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M')
    DATA = pd.read_csv(data_files_to_import[0], header = 0, sep=',', parse_dates = ['TimeStamp'], date_parser = dateparse)

    # REASSIGN INDEX TO 'Date_&_Time_Stamp' AND REMOVE COLUMN NAME
    DATA.index = pd.DatetimeIndex(DATA['TimeStamp'])
    del DATA['TimeStamp'] 
    DATA.index.name = 'Timestamp'

    # RETURN PROGRAM TO ORIGINAL FOLDER
    os.chdir(original_folder)

    return(DATA)